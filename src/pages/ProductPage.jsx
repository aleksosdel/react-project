import React from 'react';
import {Card, Col, Row} from "react-bootstrap";
import classes from "../components/MyStyle.module.css";
import {Link, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
const ProductPage = () => {
    const param = useParams()
    const id = param.id - 1
    const product = useSelector(state => state.cards[id])
    return (
        <Row className='mt-4'>
            <Col md={6}>
                <Card>
                    <Card.Img
                        variant="top"
                        src={product.img}
                    />
                </Card>
            </Col>
            <Col md={6}>
                <div className={classes.productPrice}>{product.price}&#8381;</div>
                <h1>{product.title}</h1>
                <div>{product.desc}</div>
                <Link to='/auth'>Auth</Link>
            </Col>
        </Row>
    );
};

export default ProductPage;