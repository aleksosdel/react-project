import React from 'react';
import {Button, Card, Col} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";

const ProductItem = ({prop}) => {
    const dispatch = useDispatch()
    const removeProduct = (id) => {
        dispatch({type: 'REMOVE_PRODUCT', id})
    }
    const navigate = useNavigate()
    return (
        <Col md={4} className='mt-4'>
            <Card>
                <Card.Img
                    height={150} variant="top"
                    src={prop.img}
                />
                <Card.Body>
                    <Card.Title>{prop.title}</Card.Title>
                    <Card.Text >
                        {prop.desc}
                    </Card.Text>
                    <Button
                        style={{width: '100%'}}
                        variant="danger"
                        onClick={()=> navigate('/' + prop.id)}
                    >Buy</Button>
                    <Button
                        style={{width: '100%', marginTop: "10px"}}
                        variant="danger"
                        onClick={() => removeProduct(prop.id)}
                    >Del</Button>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ProductItem;